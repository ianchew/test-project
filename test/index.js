const sum = require('../index');
const assert = require('assert');

// Use Mocha to test the sum function
describe('Sum function', () => {

	it('Takes the sum of an array of integers', () => {
		let res = sum([1,2,3,4,5]);
		assert.equal(res, 15);
	});

	it('Takes the sum of an array of floats', () => {
		let res = sum([1.5, 2.5, 3.5, 4.5]);
		assert.equal(res, 12);
	});

	it('The sum of an empty array is 0', () => {
		let res = sum([]);
		assert.equal(res, 0);
	});

	it('Fails when passed a number', () => {
		assert.throws(() => {
			sum(35);
		});
	});

});
