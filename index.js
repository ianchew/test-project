/**
 * Returns the sum of the elements of an interable container.
 *
 * @param {object} iterable - A container to sum.
 * @return {*} - The sum of the elements in the container.
 */
function sum(iterable) {
	if (!iterable.hasOwnProperty('length')) {
		throw new Error("Sum input not iterable.");
	} else if (iterable.length === 0) {
		return 0;
	} else {
		let ret = iterable[0];
		for (let i=1; i<iterable.length; ++i) {
			ret += iterable[i];
		}
		return ret;
	}
}

module.exports = sum;
